package edu.attractor.exam.controller;

import edu.attractor.exam.model.Place;
import edu.attractor.exam.model.User;
import edu.attractor.exam.repository.PlaceRepository;
import edu.attractor.exam.service.PlaceService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.security.Principal;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/")
public class PlaceController {

    @Autowired
    private PlaceRepository placeRepository;

    @Autowired
    private PlaceService placeService;

    @Value("${upload.path}")
    private String uploadPath;

    private final ModelMapper mapper= new ModelMapper();

    @GetMapping("/")
    public String greeting(Model model, Principal principal) {
        List<Place> allPlaces = (List<Place>) placeService.findAll();
        model.addAttribute( "place", allPlaces.stream()
                .map(p -> mapper.map(p, Place.class))
                .distinct()
                .collect(Collectors.toList()));
        model.addAttribute("user",principal);
        return "greeting";
    }

    @PostMapping("/main")
    public String addPlaces(@AuthenticationPrincipal User user, @RequestParam String text, @RequestParam String tag,@RequestParam("file") MultipartFile file) throws IOException {
        Place place = new Place(text, tag, user);
        if (file != null && !file.getOriginalFilename().isEmpty()) {
            File uploadDir = new File(uploadPath);
            if (!uploadDir.exists()) {
                uploadDir.mkdir();
            }
            String uuidFile = UUID.randomUUID().toString();
            String resultFilename = uuidFile + "." + file.getOriginalFilename();
            file.transferTo(new File(uploadPath + "/" + resultFilename));
            place.setFilename(resultFilename);
        }
        placeRepository.save(place);
        return "redirect:/";
    }
}
