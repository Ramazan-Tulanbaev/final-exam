package edu.attractor.exam.controller;

import edu.attractor.exam.exeption.NotFoundException;
import edu.attractor.exam.model.Establishments;
import edu.attractor.exam.model.Place;
import edu.attractor.exam.model.User;
import edu.attractor.exam.repository.EstablishmentsRepository;
import edu.attractor.exam.repository.PlaceRepository;
import edu.attractor.exam.service.EstablishmentsService;
import edu.attractor.exam.service.PlaceService;
import edu.attractor.exam.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.security.Principal;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/")
public class EstablishmentsController {
    @Autowired
    private PlaceService placeService;

    @Autowired
    private UserService userService;

    private final ModelMapper mapper= new ModelMapper();

    @Autowired
    private EstablishmentsRepository establishmentsRepository;

    @Value("${upload.path}")
    private String uploadPath;

    @GetMapping("/info/places/{id}")
    public String infoPlaces(@PathVariable Integer id, Map<String, Object> model,Principal principal){
        Place place =  placeService.getMessage(id);
        model.put("place",place);
        List<Establishments> establishments  = establishmentsRepository.findAllByPlaceId(id);
        model.put("establishments", establishments.stream().map(p -> mapper.map(p, Establishments.class)).distinct().sorted(Comparator.comparing(Establishments::getLocalDateTime, Comparator.nullsLast(Comparator.reverseOrder()))).collect(Collectors.toList()));
        List<Establishments> establishments1 = (List<Establishments>) establishmentsRepository.findAll();
        model.put( "establishments1", establishments1.stream().map(p -> mapper.map(p, Establishments.class)).distinct().sorted(Comparator.comparing(Establishments::getLocalDateTime, Comparator.nullsLast(Comparator.reverseOrder()))).collect(Collectors.toList()));
        model.put("user",principal);
        return "infoPlaces";
    }

    @PostMapping("/add/photo/{id}")
    public String addPhoto(@PathVariable Integer id,@RequestParam("file") MultipartFile file) throws IOException {
        Establishments  establishments = new Establishments();
        if (file != null && !file.getOriginalFilename().isEmpty()) {
            File uploadDir = new File(uploadPath);
            if (!uploadDir.exists()) {
                uploadDir.mkdir();
            }
            String uuidFile = UUID.randomUUID().toString();
            String resultFilename = uuidFile + "." + file.getOriginalFilename();
            file.transferTo(new File(uploadPath + "/" + resultFilename));
            establishments.setFilename(resultFilename);
        }
        establishmentsRepository.save(establishments);
        return "redirect:/info/places/{id}";
    }

    @PostMapping("/add/comment/{id}")
    public String addAnswer(@PathVariable Integer id,Establishments review, Principal principal){
        Place place = placeService.getMessage(id);
        User user = userService.getByUsername(principal.getName());
        LocalDateTime localDateTime = LocalDateTime.now();
        Establishments establishments1 = new Establishments(place, user, review.getReview(), localDateTime);
        establishmentsRepository.save(establishments1);
        return "redirect:/info/places/{id}";
    }

}
