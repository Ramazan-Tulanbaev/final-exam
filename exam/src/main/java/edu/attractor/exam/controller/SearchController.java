package edu.attractor.exam.controller;

import edu.attractor.exam.model.Place;
import edu.attractor.exam.service.PlaceService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/")
public class SearchController {
    @Autowired
    private PlaceService placeService;

    private final ModelMapper mapper= new ModelMapper();

    @GetMapping("/search")
    public String search(@PageableDefault(value = 15) Pageable pageable, String name, Model model){
        final Page<Place> text = this.placeService.getWithFilter(pageable,name);
        model.addAttribute("text",text.stream()
                .map(t-> mapper.map(t, Place.class))
                .distinct()
                .sorted(Comparator.comparing(Place::getFilename, Comparator.nullsLast(Comparator.reverseOrder())))
                .collect(Collectors.toList()));
        final Page<Place> tag = this.placeService.getWithFilter2(pageable,name);
        model.addAttribute("tag",tag.stream()
                .map(t-> mapper.map(t, Place.class))
                .distinct()
                .sorted(Comparator.comparing(Place::getFilename, Comparator.nullsLast(Comparator.reverseOrder())))
                .collect(Collectors.toList()));
        return "search";
    }
}
