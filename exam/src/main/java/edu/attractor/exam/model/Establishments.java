package edu.attractor.exam.model;

import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Establishments {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "message_id", referencedColumnName = "id")
    private Place place;

    @ManyToOne
    private User user;

    private String review;

    private String filename;

    private LocalDateTime localDateTime;

    public Establishments(Place place, User user, String review, LocalDateTime localDateTime) {
        this.place = place;
        this.user = user;
        this.review = review;
        this.localDateTime = localDateTime;
    }
}
