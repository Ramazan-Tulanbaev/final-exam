package edu.attractor.exam.service;

import edu.attractor.exam.exeption.NotFoundException;
import edu.attractor.exam.model.Place;
import edu.attractor.exam.repository.PlaceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlaceService {
    @Autowired
    private PlaceRepository placeRepository;

    public Place getMessage(Integer id) {
        return placeRepository.findById(id).orElseThrow(NotFoundException::new);
    }

    public Iterable<Place> findAll(){
        return placeRepository.findAll();
    }

    public Place save(Place place){
        return placeRepository.save(place);
    }

    public Page<Place> getWithFilter(Pageable pageable, String name) {
        Page<Place> text =null;
        if(name!=null){
            text=this.placeRepository.findAllByTextIsContaining(name,pageable);
        }
        return text;
    }

    public Page<Place> getWithFilter2(Pageable pageable, String name) {
        Page<Place> tag =null;
        if(name!=null){
            tag=this.placeRepository.findAllByTagIsContaining(name,pageable);
        }
        return tag;
    }
}
