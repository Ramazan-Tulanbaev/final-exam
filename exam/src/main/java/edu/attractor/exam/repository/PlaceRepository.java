package edu.attractor.exam.repository;

import edu.attractor.exam.model.Place;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlaceRepository extends CrudRepository<Place,Integer> {

    Page<Place> findAllByTextIsContaining(String name, Pageable pageable);

    Page<Place> findAllByTagIsContaining(String name, Pageable pageable);
}
