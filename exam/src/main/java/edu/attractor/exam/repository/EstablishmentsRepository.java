package edu.attractor.exam.repository;

import edu.attractor.exam.model.Establishments;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EstablishmentsRepository extends CrudRepository<Establishments,Integer> {
    List<Establishments> findAllByPlaceId(Integer id);
}
